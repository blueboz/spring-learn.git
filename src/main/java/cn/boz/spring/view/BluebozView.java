package cn.boz.spring.view;

import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class BluebozView implements View {
    @Override
    public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        model.forEach((k,v)->{
            StringBuilder sb = new StringBuilder();
            sb.append(k);
            sb.append("==>");
            sb.append(v);
            sb.append("\n");
            try {
                response.getOutputStream().print(sb.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

    }

    @Override
    public String getContentType() {
        return "application/xml";
    }
}
