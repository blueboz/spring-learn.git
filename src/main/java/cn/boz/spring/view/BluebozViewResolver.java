package cn.boz.spring.view;

import org.springframework.web.servlet.View;
import org.springframework.web.servlet.ViewResolver;

import java.util.Locale;

/**
 * urlbaseView的话，必须是AbstractView的子类
 */
public class BluebozViewResolver implements ViewResolver {


    @Override
    public View resolveViewName(String viewName, Locale locale) throws Exception {
        var bv=new BluebozView();
        return bv;
    }
}
