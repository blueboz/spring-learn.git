package cn.boz.spring.config;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;

import cn.boz.spring.view.BluebozView;
import cn.boz.spring.view.Pdfview;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;
import org.springframework.web.servlet.view.xml.MarshallingView;

import cn.boz.spring.interceptor.MyHandlerInterceptor;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "cn.boz.spring")
public class MyWebConfig implements WebMvcConfigurer {

	/**
	 * 添加拦截器
	 * 
	 * @param registry
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new MyHandlerInterceptor());
		// registry.addInterceptor(new
		// MyHandlerInterceptor()).addPathPatterns("/**").excludePathPatterns("");
	}

	/**
	 * 设置静态资源的路径处理器 用于处理静态资源 urlPattern 表示需要对什么样的url进行拦截 resourceLocation
	 * 表示资源相对于webroot目录的位置 cache 表示缓存的时长
	 * 
	 * @param registry
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/assets/**").addResourceLocations("/assets/").setCachePeriod(31288399);
	}

	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {
		//var marshallingView = new MarshallingView();
		//ContentNegot
		View[] vs= {new MappingJackson2JsonView(),new BluebozView(),new Pdfview()};
		registry.enableContentNegotiation(vs);
		registry.jsp("WEB-INF/page/", ".jsp");
		WebMvcConfigurer.super.configureViewResolvers(registry);
	}

	@Bean
	public SimpleMappingExceptionResolver simpleMappingExceptionResolver() {
		SimpleMappingExceptionResolver b = new SimpleMappingExceptionResolver();
		Properties mappings = new Properties();
		mappings.put("org.springframework.web.servlet.PageNotFound", "httpMessageNotReadable");
		mappings.put("org.springframework.dao.DataAccessException", "httpMessageNotReadable");
		mappings.put("org.springframework.transaction.TransactionException", "httpMessageNotReadable");
		mappings.put("java.lang.ArithmeticException", "httpMessageNotReadable");
		mappings.put("org.springframework.http.converter.HttpMessageNotReadableException", "httpMessageNotReadable");
		b.setExceptionMappings(mappings);
		return b;
	}

	@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		// TODO Auto-generated method stub
		Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder().indentOutput(true)
				.dateFormat(new SimpleDateFormat("yyyy-MM-dd"));
		converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
		converters.add(new MappingJackson2XmlHttpMessageConverter(builder.createXmlMapper(true).build()));
		WebMvcConfigurer.super.extendMessageConverters(converters);
	}
	
	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		WebMvcConfigurer.super.configureContentNegotiation(configurer);
	}

}
