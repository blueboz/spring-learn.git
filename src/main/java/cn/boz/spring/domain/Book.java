package cn.boz.spring.domain;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

public class Book {

	@NotEmpty(message="书名不能为空")
	private String bookName;
	
	@NotNull(message="ISBN号不能为空")
	private String bookIsbn;
	
	@DecimalMin(value="0.1",message="单价最低0.1")
	private double price;

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookIsbn() {
		return bookIsbn;
	}

	public void setBookIsbn(String bookIsbn) {
		this.bookIsbn = bookIsbn;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "Book [bookName=" + bookName + ", bookIsbn=" + bookIsbn + ", price=" + price + "]";
	}
	
}
