package cn.boz.spring.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cn.boz.spring.domain.Book;

/**
 * Rest风格的控制器
 */
@RestController
public class MyRestController {
	
	@RequestMapping(value="/book",method=RequestMethod.GET)
	public void addBook(@RequestBody @Valid Book book) {
		System.out.println(book.toString());
	}
	
	@ExceptionHandler(ArithmeticException.class)
	@RequestMapping("value")
	public String tryException(){
		return "11";
	}
	
	@RequestMapping("/error")
	public Map<String,Object> somethingWrong() {
		var map=new HashMap<String,Object>();
		map.put("exception", "Something wrong");
		map.put("exception2", "Something wrong");
		map.put("time", new Date());
		return map;
	}
}
