package cn.boz.spring.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import cn.boz.spring.domain.Book;

/**
 * 非Rest风格的控制器
 */
@Controller
public class MyController {

	@Autowired
	private ApplicationContext ac;
	@RequestMapping("/dashboard")
	public ModelAndView sayHello(){
		var mav=new ModelAndView("dashboard");
		mav.addObject("user", Math.random());
		return mav;
	}

	@RequestMapping("/userinfo")
	public ModelAndView userInfo(){
		var mav=new ModelAndView("userinfo");
		mav.addObject("user", "blueboz");
		mav.addObject("age",18);
		mav.addObject("gender","male");
		return mav;
	}

	@RequestMapping("/findUser")
	public ModelAndView findUser(){
		var mav=new ModelAndView("findUser");
		var u=new User();
		u.setUsername("Jay Chou");
		u.setAge("18");
		u.setBirthday(new Date());
		u.setGender("Male");
		mav.addObject("userinfo",u);
		return mav;
	}

	class User{
		private String username;
		private String age;
		private String gender;
		private Date birthday;
		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getAge() {
			return age;
		}

		public void setAge(String age) {
			this.age = age;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public Date getBirthday() {
			return birthday;
		}

		public void setBirthday(Date birthday) {
			this.birthday = birthday;
		}
	}
	
	@RequestMapping("/httpMessageNotReadable")
	public String httpMessageNotReadable() {
		return "httpMessageNotReadable";
	}
	
	@RequestMapping(value="/book2",method=RequestMethod.GET)
	public String addBook(@RequestBody @Valid Book book) {
		System.out.println(book.toString());
		return "book2";
	}
	
	@RequestMapping(value="showviews")
	public Map showMeAboutViews() {
		Map<String, View> beans = ac.getBeansOfType(View.class);
		beans.forEach((k,v)->{
			System.out.println(k);
		});

		return beans;
	}
	

}
